program
int: x, y, z ;

begin
input x, y;
z := x + y;
output z;
end;
