/*************************************************************
 * 'ReadMe.txt'
 * This is a generic ReadMe file
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--11:04:36
**/

/****************************************************************
 * Program:''
 * Language: Lexical Analysis Flex/Bison 
**/

(\+|\-){digit}+\.("E"(\+|\-)+{digit}+)+ 

For project 2,  you should extend project 1.
1)	When an IDENTIFIER is recognized you should
a.	Install the lexeme (strdup(yytest)) in the symbol table using kr.symtab.c from /class/csce531/Table 
b.	For the union  for defining the Stack attribute (and yylval) as a starter you could use 
%union {
   int ival;
   struct nlist *place; 
}
Where struct nlist * is a pointer into the symbol table.
c.	 So when an identifier such as “xbar” is recognized, “xbar” the lexeme will be installed in the symbol table vid tmp = lookup(strdup(yytest)); and yylval.place = tmp before returning the token code IDENTIFIER.

2)	Using Postfix from /class/csce531 for ideas extend to generate code (quadruples) for expressions.

3)	Recognize more tokens:
a.	all of the keywords in the grammar on page 74 of the handout: (Note some of the material in the BNF really belongs in the flex file, e.g. integer
i.	program begin end  input output if then else while
ii.	symbols/punctuation ‘,’   ‘:’    ‘;’    ‘+’   ‘*’   ‘-‘   ‘(‘     ‘)’  “:=”
iii.	comparison operators  ‘<’   ‘=’   “!=”   ‘>’ for each of these comparison operators you should
1.	return the token code  RELOP and set yylval to 1 for ‘<’, 2 for ‘=’, …
4)	Extend the grammar in your bison file to parse (Without generated code except for expressions):
a.	Statement, and all kinds of statements
b.	Comparison
c.	Expression, you can use the ambiguous grammar E -> E+E|E*E|E-E| INTEGER  | IDENTIFIER | ‘(‘ E ‘)’ if you like or the grammar with factor and operand on page 74
d.	Note everything below operand in this grammar belongs in the lex file.
5)	For declarations you should use D int idlist ‘;’ (or one of the variants discussed in class today Lecture 15 on website) instead of declaration identifier [, identifier] … : integer ;  
6)	You then check for undefined variables when an identifier is encountered as part of an expression and printf an “Undeclared variable” error message including the linenumber (yylineno.) 



/****************************************************************
 * End 'ReadMe.txt'
**/

