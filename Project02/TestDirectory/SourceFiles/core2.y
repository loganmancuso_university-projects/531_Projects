%{
/*****************************************************************
 * 'core2.y'
 * core2.y is a grammer specification file
 * tar cvfz project1.tgz   project1
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 03-30-2018--17:44:52
 * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED
**/

yydebug=1;
#include <stdio.h>
#include <stdlib.h>
#include "kr.symtab.c"

#define INTEGER_TYPE;

%}
%union {
	int relop;
	int ival;
	char real;
	struct nlist *place;
	INTEGER_TYPE = 1000;
}

%token PROGRAM BEGINT INPUTT OUTPUTT ENDT /*core specific*/
%token IF THEN ENDIF ELSE WHILE LOOP ENDLOOP /*relational statements*/
%token INTTYPE DOUBLETYPE FLOATTYPE REALTYPE /*types*/
%token DOUBLE FLOAT /*values*/
%token <place> ID
%token <ival> INT
%token <real> REAL
%token <relop> RELOP
%token LPAREN RPAREN ASSOP BAR /*relop*/
%token PLUS MINUS TIMES DIV MOD /*math operations*/
%token SEMICOLON COLON DOT COMA DBQUOTE SQUOTE /*general*/
%token NEWLINE /*whitespace*/

%%
/*program line in core*/
line	:	program NEWLINE 	{
									printf("recognized a program \n");
									exit(0);
								}
	;
program	:	PROGRAM NEWLINE  declaration BEGINT NEWLINE statement ENDT SEMICOLON
	;
statement	:	body NEWLINE statement
	|	body NEWLINE
	;
/*core specific assignments*/
body	:	input
	|	output
	|	assignment
	;
input	:	INPUTT idlist SEMICOLON
	;
output	:	OUTPUTT idlist SEMICOLON
	;
declaration	: type COLON idlist SEMICOLON NEWLINE declaration {yylval.place -> type = INTEGER_TYPE;}
	|	type COLON idlist SEMICOLON NEWLINE 
	;
idlist	:	ID COMA idlist
	|	ID
	;
assignment	:	ID ASSOP expr
	;
expr	:	expr PLUS sub
	|	sub
	;
sub : sub MINUS term
	|	term
	;
term	:	term TIMES divisor
	|	divisor
	;
divisor : divisor DIV factor
	|	factor
	;
factor	:	LPAREN expr RPAREN	
	|	value
	|	ID
	;
ifstatement	:	IF comparison THEN statement ENDIF SEMICOLON
	|	IF comparison THEN statement ELSE statement ENDIF SEMICOLON
	;
loop	:	WHILE comparison LOOP statement ENDLOOP SEMICOLON
	;
comparison	:	LPAREN operand RELOP operand RPAREN
	;
operand	:	value
	|	ID
	|	expr
	;
value :	INT
	|	DOUBLE
	|	FLOAT
	|	REAL
	;
type	:	INTTYPE
	|	DOUBLETYPE
	|	FLOATTYPE
	|	REALTYPE
	;

%%
/****************************************************************
 * End 'core2.y'
**/
